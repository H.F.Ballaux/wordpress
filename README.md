# Installation du site WordPress



## Prérequis

- WampServer ou equivalent
- PHP version 7.3 ou supérieure
- MySQL version 5.6 ou supérieure.
- Git project: https://gitlab.com/H.F.Ballaux/wordpress.git

## Installation via Git Repository:

Dans votre dossier 'www' de Wamp64 (ou equivalent selon le serveur utilisé), vous allez y cloner le projet git, via la 
commande suivante, dans le terminal de l'os ou dans git bash

```
git clone https://gitlab.com/H.F.Ballaux/wordpress.git
```
Accedez ensuite aux dossier 'wordpress', dedans vous trouverez le dossier 'Ressources' contenant la Base de Données du site (wordpress.sql), il vous faudra l'importer dans MySQL via phpMyadmin

Pour ce faire dans phpMyadmin vous allez créer une nouvelle base de données,
retourner dans le menu et via l'onglet 'importer' vous aurez la possibilitée de sélectionner un fichier vous y mettrez le fichier 'wordrepss.sql'

Il faudra ensuite configurer le fichier 'wp-config.php' pour ce faire faites une copie du fichier
'wp-config-sample.php', renommez le fichier 'wp-config.php' et ouvrez le dans un editeur de texte.

Dans ce fichier il faudra modifier les champs suivant :
- DB_NAME : Entrez le nom de la base de données  (celle créée dans phpMyadmin)
- DB_USER : Entrez le nom d'utilisateur de la base de données
- DB_PASSWORD : Entrez le mots-de-passes d'utilisateur de la base de données
- DB_HOST : Entrez addresse d'hébergement de la base de données

Il ne vous reste plus qu'a enregistrer les modifications apportées au fichier et démarrer votre nouveau site a l'addresse suivante:

- http://localhost/wordpress

remplacez 'localhost' par l'addresse d'hébergement de la base de données référencée dans le fichier 'wp-config.php'

## Name
TvCom WordPress Project

## Description
examens CMS BES WEB 1

## Authors and acknowledgment
- Hector Fabio Ballaux 
- WordPress.org

## Project status
Done