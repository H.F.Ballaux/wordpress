msgid ""
msgstr ""
"Project-Id-Version: w3csspress\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-31 15:45+0000\n"
"PO-Revision-Date: 2022-10-31 15:51+0000\n"
"Last-Translator: \n"
"Language-Team: Greek\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.6.3; wp-6.0.3\n"
"X-Domain: w3csspress"

#. border and color
#: classes/class-w3csspress-constants.php:222
#, fuzzy, php-format
msgid "%1$s %2$s"
msgstr "%1$s %2$s"

#. arrow
#. arrow and text
#: attachment.php:62 template-parts/nav/below.php:19
#, fuzzy, php-format
msgid "%s older"
msgstr "%s παλαιότερα"

#: inc/patterns/notfound.php:22
#, fuzzy
msgid "404"
msgstr "404"

#: inc/block-patterns.php:23
#, fuzzy
msgid "404 not found"
msgstr "404 δεν βρέθηκε"

#: inc/patterns/query.php:43
#, fuzzy
msgid "add text or blocks to display when the query returns no results."
msgstr ""
"να προσθέσετε κείμενο ή μπλοκ που θα εμφανίζονται όταν το ερώτημα δεν "
"επιστρέφει αποτελέσματα."

#: classes/class-w3csspress-constants.php:36
#, fuzzy
msgid "all borders"
msgstr "όλα τα σύνορα"

#: classes/class-w3csspress-constants.php:57
#, fuzzy
msgid "amber"
msgstr "κεχριμπάρι"

#. color of the container
#: inc/patterns/badge.php:30
#, fuzzy, php-format
msgid "badge %s"
msgstr "σήμα %s"

#: inc/block-patterns.php:24
#, fuzzy
msgid "badges"
msgstr "σήματα"

#: inc/patterns/navigation.php:37
#, fuzzy
msgid "basic navigation"
msgstr "βασική πλοήγηση"

#: classes/class-w3csspress-constants.php:58
#, fuzzy
msgid "black"
msgstr "μαύρο"

#: classes/class-w3csspress-constants.php:59
#, fuzzy
msgid "blue"
msgstr "μπλε"

#: classes/class-w3csspress-constants.php:60
#, fuzzy
msgid "blue grey"
msgstr "μπλε γκρι"

#. border and direction
#: classes/class-w3csspress-constants.php:27
#, fuzzy, php-format
msgid "border %s"
msgstr "σύνορα %s"

#: inc/images.php:134
#, fuzzy
msgid "border images on the page."
msgstr "εικόνες περιθωρίου στη σελίδα."

#: inc/patterns/image.php:58
#, fuzzy
msgid "bordered image"
msgstr "περιθωριοποιημένη εικόνα"

#: inc/images.php:133
#, fuzzy
msgid "bordered images"
msgstr "εικόνες με περιθώρια"

#: inc/block-patterns.php:25
#, fuzzy
msgid "borders"
msgstr "σύνορα"

#: classes/class-w3csspress-constants.php:204
#, fuzzy
msgid "bottom"
msgstr "κάτω"

#: classes/class-w3csspress-constants.php:61
#, fuzzy
msgid "brown"
msgstr "καφέ"

#. color of the card
#: inc/patterns/card.php:40
#, fuzzy, php-format
msgid "card %s"
msgstr "κάρτα %s"

#: inc/patterns/image.php:70
#, fuzzy
msgid "card image"
msgstr "εικόνα κάρτας"

#: inc/images.php:145
#, fuzzy
msgid "card images"
msgstr "εικόνες κάρτας"

#. color of the card
#: inc/patterns/card.php:54
#, fuzzy, php-format
msgid "card-2 %s"
msgstr "κάρτα-2 %s"

#. color of the card
#: inc/patterns/card.php:68
#, fuzzy, php-format
msgid "card-4 %s"
msgstr "κάρτα-4 %s"

#: inc/block-patterns.php:26 inc/patterns/card.php:26
#, fuzzy
msgid "cards"
msgstr "κάρτες"

#: template-parts/entry/footer.php:16
#, fuzzy
msgid "categories: "
msgstr "κατηγορίες"

#. index for headings
#: inc/colors.php:143
#, fuzzy, php-format
msgid "change color of h%s."
msgstr "αλλάξτε το χρώμα του h%s."

#: inc/fonts.php:192
#, fuzzy
msgid "change font family of headings."
msgstr "αλλαγή οικογένειας γραμματοσειράς των επικεφαλίδων."

#: inc/fonts.php:179 inc/fonts.php:204
#, fuzzy
msgid "change font family of website."
msgstr "αλλάξτε την οικογένεια γραμματοσειρών του ιστότοπου."

#: inc/fonts.php:229
#, fuzzy
msgid "change font size of div."
msgstr "αλλαγή μεγέθους γραμματοσειράς του div."

#. index for headings
#: inc/fonts.php:397
#, fuzzy, php-format
msgid "change font size of h%s."
msgstr "αλλαγή μεγέθους γραμματοσειράς του h%s."

#: inc/fonts.php:242
#, fuzzy
msgid "change font size of inputs."
msgstr "αλλαγή μεγέθους γραμματοσειράς των εισόδων."

#: inc/fonts.php:281
#, fuzzy
msgid "change font size of ordered list."
msgstr "αλλαγή μεγέθους γραμματοσειράς της διατεταγμένης λίστας."

#: inc/fonts.php:216
#, fuzzy
msgid "change font size of paragraphs."
msgstr "να αλλάξετε το μέγεθος της γραμματοσειράς των παραγράφων."

#: inc/fonts.php:255
#, fuzzy
msgid "change font size of tables."
msgstr "αλλαγή μεγέθους γραμματοσειράς των πινάκων."

#: inc/fonts.php:268
#, fuzzy
msgid "change font size of unordered list."
msgstr "αλλαγή μεγέθους γραμματοσειράς της μη ταξινομημένης λίστας."

#: inc/fonts.php:307
#, fuzzy
msgid "change font weight of div."
msgstr "αλλάξτε το βάρος γραμματοσειράς του div."

#. index for headings
#: inc/fonts.php:418
#, fuzzy, php-format
msgid "change font weight of h%s."
msgstr "αλλάξτε το βάρος της γραμματοσειράς h%s."

#: inc/fonts.php:320
#, fuzzy
msgid "change font weight of inputs."
msgstr "αλλαγή βάρους γραμματοσειράς των εισροών."

#: inc/fonts.php:359
#, fuzzy
msgid "change font weight of ordered list."
msgstr "αλλαγή του βάρους γραμματοσειράς της διατεταγμένης λίστας."

#: inc/fonts.php:294
#, fuzzy
msgid "change font weight of paragraphs."
msgstr "αλλάξτε το βάρος της γραμματοσειράς των παραγράφων."

#: inc/fonts.php:333
#, fuzzy
msgid "change font weight of tables."
msgstr "αλλαγή βάρους γραμματοσειράς των πινάκων."

#: inc/fonts.php:346
#, fuzzy
msgid "change font weight of unordered list."
msgstr "αλλαγή του βάρους γραμματοσειράς της μη ταξινομημένης λίστας."

#: inc/colors.php:122
#, fuzzy
msgid "change link color"
msgstr "αλλαγή χρώματος συνδέσμου"

#: inc/colors.php:108
#, fuzzy
msgid "change theme text color"
msgstr "αλλαγή χρώματος κειμένου θέματος"

#: classes/class-w3csspress-constants.php:190
#, fuzzy
msgid "circle"
msgstr "κύκλος"

#: inc/images.php:121
#, fuzzy
msgid "circle images"
msgstr "εικόνες κύκλων"

#: inc/images.php:122
#, fuzzy
msgid "circle images on the page."
msgstr "κυκλικές εικόνες στη σελίδα."

#: inc/patterns/image.php:46
#, fuzzy
msgid "circular image"
msgstr "κυκλική εικόνα"

#: inc/block-patterns.php:27
#, fuzzy
msgid "colors"
msgstr "χρώματα"

#: comments.php:65
#, fuzzy
msgid "comment:"
msgstr "σχόλιο:"

#: template-parts/entry/footer.php:20
#, fuzzy
msgid "comments"
msgstr "σχόλια"

#: comments.php:53
#, fuzzy
msgctxt "comments count"
msgid "Trackback or Pingback"
msgid_plural "Trackbacks and Pingbacks"
msgstr[0] "Trackback ή Pingback"
msgstr[1] "Trackbacks και Pingbacks"

#. color of the container
#: inc/patterns/container.php:40
#, fuzzy, php-format
msgid "container %s"
msgstr "δοχείο %s"

#: inc/block-patterns.php:28 inc/patterns/container.php:26
#, fuzzy
msgid "containers"
msgstr "εμπορευματοκιβώτια"

#: inc/content.php:58
#, fuzzy
msgid "content options"
msgstr "επιλογές περιεχομένου"

#: classes/class-w3csspress-constants.php:156
#, fuzzy
msgid "cursive"
msgstr "καλλιγραφική"

#: inc/content.php:59
#, fuzzy
msgid "customize content options here."
msgstr "προσαρμόστε τις επιλογές περιεχομένου εδώ."

#: inc/fonts.php:32
#, fuzzy
msgid "customize fonts options here."
msgstr "προσαρμόστε τις επιλογές γραμματοσειρών εδώ."

#: inc/images.php:28
#, fuzzy
msgid "customize images options here."
msgstr "προσαρμόστε τις επιλογές εικόνων εδώ."

#: inc/layout.php:32
#, fuzzy
msgid "customize layout options here."
msgstr "προσαρμόστε τις επιλογές διάταξης εδώ."

#: inc/speed.php:28
#, fuzzy
msgid "customize speed options here."
msgstr "προσαρμόστε τις επιλογές ταχύτητας εδώ."

#: classes/class-w3csspress-constants.php:62
#, fuzzy
msgid "cyan"
msgstr "κυανό"

#: classes/class-w3csspress-constants.php:93
#: classes/class-w3csspress-constants.php:94
#: classes/class-w3csspress-constants.php:95
#: classes/class-w3csspress-constants.php:96
#: classes/class-w3csspress-constants.php:97
#, fuzzy
msgid "dark"
msgstr "dark"

#: classes/class-w3csspress-constants.php:63
#, fuzzy
msgid "dark grey"
msgstr "σκούρο γκρι"

#: classes/class-w3csspress-constants.php:64
#, fuzzy
msgid "deep orange"
msgstr "βαθύ πορτοκαλί"

#: classes/class-w3csspress-constants.php:65
#, fuzzy
msgid "deep purple"
msgstr "βαθύ μωβ"

#: classes/class-w3csspress-constants.php:55
#: classes/class-w3csspress-constants.php:98
#: classes/class-w3csspress-constants.php:115
#: classes/class-w3csspress-constants.php:135
#: classes/class-w3csspress-constants.php:152
#: classes/class-w3csspress-constants.php:168
#: classes/class-w3csspress-constants.php:184
#, fuzzy
msgid "default"
msgstr "προεπιλογή"

#: inc/speed.php:93
#, fuzzy
msgid "disable jQuery"
msgstr "απενεργοποίηση της jQuery"

#: inc/speed.php:94
#, fuzzy
msgid "disable or enable jQuery (do only if you know what you are doing)."
msgstr ""
"απενεργοποιήστε ή ενεργοποιήστε την jQuery (κάντε το μόνο αν ξέρετε τι "
"κάνετε)."

#: inc/speed.php:141
#, fuzzy
msgid "enable block styles"
msgstr "ενεργοποίηση στυλ μπλοκ"

#: inc/speed.php:142
#, fuzzy
msgid "enable block styles."
msgstr "ενεργοποιήστε τα στυλ μπλοκ."

#: inc/speed.php:105
#, fuzzy
msgid "enable Dashicons"
msgstr "ενεργοποίηση Dashicons"

#: inc/speed.php:106
#, fuzzy
msgid "enable Dashicons on the frontend."
msgstr "ενεργοποιήστε τα Dashicons στο frontend."

#: inc/speed.php:153
#, fuzzy
msgid "enable full site editor"
msgstr "ενεργοποιήστε τον πλήρη επεξεργαστή ιστότοπου"

#: inc/speed.php:117
#, fuzzy
msgid "enable Gutenberg for posts"
msgstr "ενεργοποιήστε το Gutenberg για τις αναρτήσεις"

#: inc/speed.php:129
#, fuzzy
msgid "enable Gutenberg for widgets"
msgstr "ενεργοποιήστε το Gutenberg για widgets"

#: inc/speed.php:118
#, fuzzy
msgid "enable Gutenberg to edit posts."
msgstr "ενεργοποιήστε το Gutenberg για να επεξεργαστείτε τις δημοσιεύσεις."

#: inc/speed.php:130
#, fuzzy
msgid "enable Gutenberg to edit widgets."
msgstr "ενεργοποιήστε το Gutenberg για να επεξεργαστείτε τα widgets."

#: inc/speed.php:154
#, fuzzy
msgid "enable the full site editor."
msgstr "ενεργοποιήστε τον πλήρη επεξεργαστή ιστότοπου."

#: inc/fonts.php:31
#, fuzzy
msgid "fonts options"
msgstr "επιλογές γραμματοσειρών"

#: functions.php:364
#, fuzzy
msgid "foot widgets sidebar"
msgstr "πλευρική μπάρα widgets ποδιών"

#: inc/patterns/footer.php:28
#, fuzzy
msgid "footer"
msgstr "υποσέλιδο"

#: inc/content.php:79
#, fuzzy
msgid "footer content"
msgstr "περιεχόμενο υποσέλιδου"

#: inc/block-patterns.php:29
#, fuzzy
msgid "footers"
msgstr "υποσέλιδα"

#: classes/class-w3csspress-constants.php:172
#, fuzzy
msgid "four columns"
msgstr "τέσσερις στήλες"

#. Name of the template
#, fuzzy
msgid "Full-Width"
msgstr "Πλήρες πλάτος"

#: inc/images.php:169
#, fuzzy
msgid "grayscale images"
msgstr "εικόνες κλίμακας του γκρι"

#: classes/class-w3csspress-constants.php:66
#, fuzzy
msgid "green"
msgstr "πράσινο"

#: classes/class-w3csspress-constants.php:67
#, fuzzy
msgid "grey"
msgstr "γκρι"

#: inc/patterns/image.php:94
#, fuzzy
msgid "greyscale image"
msgstr "εικόνα κλίμακας του γκρι"

#: inc/layout.php:96
#, fuzzy
msgid "grid layout setting"
msgstr "ρύθμιση διάταξης πλέγματος"

#. index for headings
#: inc/colors.php:138
#, fuzzy, php-format
msgid "h%s color"
msgstr "h%s χρώμα"

#: functions.php:355
#, fuzzy
msgid "head widgets sidebar"
msgstr "head widgets sidebar"

#: inc/patterns/header.php:21
#, fuzzy
msgid "header"
msgstr "επικεφαλίδα"

#: inc/images.php:193
#, fuzzy
msgid "header thumbnail"
msgstr "μικρογραφία κεφαλίδας"

#: inc/block-patterns.php:30
#, fuzzy
msgid "headers"
msgstr "επικεφαλίδες"

#: inc/patterns/header.php:51
#, fuzzy
msgid "homepage header"
msgstr "επικεφαλίδα της αρχικής σελίδας"

#: inc/block-patterns.php:31
#, fuzzy
msgid "hover"
msgstr "hover"

#. hover border and color
#: classes/class-w3csspress-constants.php:244
#, fuzzy, php-format
msgid "hover %1$s %2$s"
msgstr "hover %1$s %2$s"

#. Author URI of the theme
#, fuzzy
msgid "https://github.com/matteomarchiori"
msgstr "https://github.com/matteomarchiori"

#. URI of the theme
#, fuzzy
msgid "https://github.com/matteomarchiori/w3csspress"
msgstr "https://github.com/matteomarchiori/w3csspress"

#: inc/patterns/image.php:22
#, fuzzy
msgid "image"
msgstr "εικόνα"

#: inc/block-patterns.php:32
#, fuzzy
msgid "images"
msgstr "εικόνες"

#: inc/images.php:27
#, fuzzy
msgid "images options"
msgstr "επιλογές εικόνων"

#: inc/images.php:146
#, fuzzy
msgid "images with card effect."
msgstr "εικόνες με εφέ κάρτας."

#: inc/images.php:170
#, fuzzy
msgid "images with grayscale effect."
msgstr "εικόνες με το εφέ της κλίμακας του γκρι."

#: inc/images.php:158
#, fuzzy
msgid "images with opacity effect."
msgstr "εικόνες με αδιαφάνεια."

#: inc/images.php:182
#, fuzzy
msgid "images with sepia effect."
msgstr "εικόνες με εφέ σέπια."

#: classes/class-w3csspress-constants.php:68
#, fuzzy
msgid "indigo"
msgstr "indigo"

#: classes/class-w3csspress-constants.php:123
#, fuzzy
msgid "jumbo"
msgstr "jumbo"

#: classes/class-w3csspress-constants.php:69
#, fuzzy
msgid "khaki"
msgstr "χακί"

#: classes/class-w3csspress-constants.php:119
#, fuzzy
msgid "large"
msgstr "μεγάλο"

#: inc/layout.php:31
#, fuzzy
msgid "layout options"
msgstr "επιλογές διάταξης"

#: classes/class-w3csspress-constants.php:205
#, fuzzy
msgid "left"
msgstr "αριστερά"

#: inc/patterns/sidebar.php:26
#, fuzzy
msgid "left sidebar"
msgstr "αριστερή πλευρική μπάρα"

#: classes/class-w3csspress-constants.php:99
#: classes/class-w3csspress-constants.php:100
#: classes/class-w3csspress-constants.php:101
#: classes/class-w3csspress-constants.php:102
#: classes/class-w3csspress-constants.php:103
#, fuzzy
msgid "light"
msgstr "φως"

#: classes/class-w3csspress-constants.php:70
#, fuzzy
msgid "light blue"
msgstr "γαλάζιο"

#: classes/class-w3csspress-constants.php:71
#, fuzzy
msgid "light green"
msgstr "ανοιχτό πράσινο"

#: classes/class-w3csspress-constants.php:72
#, fuzzy
msgid "lime"
msgstr "ασβέστης"

#: inc/colors.php:121
#, fuzzy
msgid "link color"
msgstr "χρώμα συνδέσμου"

#: functions.php:90
#, fuzzy
msgid "main menu"
msgstr "κύριο μενού"

#. Author of the theme
#, fuzzy
msgid "matteomarchiori97"
msgstr "matteomarchiori97"

#: classes/class-w3csspress-constants.php:118
#, fuzzy
msgid "medium"
msgstr "medium"

#: functions.php:147
#, fuzzy
msgid "menu"
msgstr "μενού"

#: classes/class-w3csspress-constants.php:155
#, fuzzy
msgid "monospace"
msgstr "monospace"

#: inc/block-patterns.php:33
#, fuzzy
msgid "navigation"
msgstr "πλοήγηση"

#. arrow
#. arrow and text
#: attachment.php:75 template-parts/nav/below.php:24
#, fuzzy, php-format
msgid "newer %s"
msgstr "νεότερο %s"

#: 404.php:19
#, fuzzy
msgid "Not Found"
msgstr "Δεν βρέθηκε"

#: inc/patterns/notfound.php:25
#, fuzzy
msgid "not found"
msgstr "δεν βρέθηκε"

#: search.php:62
#, fuzzy
msgid "Nothing Found"
msgstr "Δεν βρέθηκε τίποτα"

#: 404.php:22
#, fuzzy
msgid "Nothing found for the requested page. Try a search instead?"
msgstr "Δεν βρέθηκε τίποτα για τη ζητούμενη σελίδα. Δοκιμάστε μια αναζήτηση"

#: inc/patterns/notfound.php:29
#, fuzzy
msgid "nothing found for the requested page. Try a search instead?"
msgstr "δεν βρέθηκε τίποτα για τη ζητούμενη σελίδα. Δοκιμάστε μια αναζήτηση"

#: classes/class-w3csspress-constants.php:169
#, fuzzy
msgid "one column"
msgstr "μία στήλη"

#: inc/images.php:157
#, fuzzy
msgid "opacity images"
msgstr "εικόνες αδιαφάνειας"

#: inc/patterns/image.php:82
#, fuzzy
msgid "opaque image"
msgstr "αδιαφανής εικόνα"

#: classes/class-w3csspress-constants.php:73
#, fuzzy
msgid "orange"
msgstr "πορτοκαλί"

#: inc/patterns/panel.php:26
#, fuzzy
msgid "panel"
msgstr "πάνελ"

#. color of the panel
#: inc/patterns/panel.php:40
#, fuzzy, php-format
msgid "panel %s"
msgstr "πάνελ %s"

#: inc/block-patterns.php:34
#, fuzzy
msgid "panels"
msgstr "πάνελ"

#: classes/class-w3csspress-constants.php:74
#, fuzzy
msgid "pink"
msgstr "ροζ"

#: inc/images.php:205
#, fuzzy
msgid "post thumbnail"
msgstr "μικρογραφία ανάρτησης"

#: inc/images.php:194
#, fuzzy
msgid "posts get the thumbnail as header image if they have one."
msgstr "οι δημοσιεύσεις παίρνουν τη μικρογραφία ως εικόνα κεφαλίδας αν έχουν."

#: inc/images.php:206
#, fuzzy
msgid "posts show the thumbnail if they have."
msgstr "οι αναρτήσεις εμφανίζουν τη μικρογραφία αν έχουν."

#: functions.php:337
#, fuzzy
msgid "primary sidebar"
msgstr "πρωτεύουσα πλευρική μπάρα"

#: classes/class-w3csspress-constants.php:75
#, fuzzy
msgid "purple"
msgstr "μοβ"

#: inc/block-patterns.php:35
#, fuzzy
msgid "query"
msgstr "ερώτημα"

#: inc/patterns/query.php:22
#, fuzzy
msgid "query loop"
msgstr "βρόχος ερωτήματος"

#: inc/patterns/query.php:33
#, fuzzy
msgid "read all"
msgstr "διαβάστε τα όλα"

#: classes/class-w3csspress-constants.php:76
#, fuzzy
msgid "red"
msgstr "κόκκινο"

#: classes/class-w3csspress-constants.php:203
#, fuzzy
msgid "right"
msgstr "δεξιά"

#: inc/patterns/sidebar.php:36
#, fuzzy
msgid "right sidebar"
msgstr "δεξιά πλαϊνή μπάρα"

#: inc/images.php:110
#, fuzzy
msgid "round images on the page."
msgstr "στρογγυλές εικόνες στη σελίδα."

#: classes/class-w3csspress-constants.php:187
#, fuzzy
msgid "round large"
msgstr "στρογγυλό μεγάλο"

#: classes/class-w3csspress-constants.php:186
#, fuzzy
msgid "round medium"
msgstr "στρογγυλό μεσαίο"

#: classes/class-w3csspress-constants.php:185
#, fuzzy
msgid "round small"
msgstr "στρογγυλό μικρό"

#: classes/class-w3csspress-constants.php:188
#, fuzzy
msgid "round xlarge"
msgstr "στρογγυλό xlarge"

#: classes/class-w3csspress-constants.php:189
#, fuzzy
msgid "round xxlarge"
msgstr "στρογγυλό xxlarge"

#: inc/patterns/image.php:34
#, fuzzy
msgid "rounded image"
msgstr "στρογγυλεμένη εικόνα"

#: inc/images.php:109
#, fuzzy
msgid "rounded images"
msgstr "στρογγυλεμένες εικόνες"

#: classes/class-w3csspress-constants.php:154
#, fuzzy
msgid "sans serif"
msgstr "sans serif"

#: inc/block-patterns.php:36 inc/patterns/search.php:25
#: inc/patterns/search.php:25
#, fuzzy
msgid "search"
msgstr "αναζήτηση"

#: inc/patterns/search.php:22
#, fuzzy
msgid "search bar"
msgstr "γραμμή αναζήτησης"

#. search text completed with query text
#: search.php:38
#, fuzzy, php-format
msgid "search results for: %s"
msgstr "αποτελέσματα αναζήτησης για: %s"

#: functions.php:346
#, fuzzy
msgid "secondary sidebar"
msgstr "δευτερεύουσα πλευρική μπάρα"

#: inc/colors.php:79
#, fuzzy
msgid "select color theme"
msgstr "επιλέξτε θέμα χρώματος"

#: inc/fonts.php:228
#, fuzzy
msgid "select div font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς div"

#: inc/fonts.php:306
#, fuzzy
msgid "select div font weight"
msgstr "επιλέξτε βάρος γραμματοσειράς div"

#: inc/fonts.php:178
#, fuzzy
msgid "select font family"
msgstr "επιλέξτε οικογένεια γραμματοσειράς"

#. index for headings
#: inc/fonts.php:392
#, fuzzy, php-format
msgid "select h%s font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς h%s"

#. index for headings
#: inc/fonts.php:413
#, fuzzy, php-format
msgid "select h%s font weight"
msgstr "select h%s βάρος γραμματοσειράς"

#: inc/fonts.php:241
#, fuzzy
msgid "select input font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς εισόδου"

#: inc/fonts.php:319
#, fuzzy
msgid "select input font weight"
msgstr "επιλέξτε βάρος γραμματοσειράς εισόδου"

#: inc/layout.php:70
#, fuzzy
msgid "select layout"
msgstr "επιλέξτε διάταξη"

#: inc/fonts.php:280
#, fuzzy
msgid "select ordered list font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς διατεταγμένης λίστας"

#: inc/fonts.php:358
#, fuzzy
msgid "select ordered list font weight"
msgstr "επιλέξτε βάρος γραμματοσειράς διατεταγμένης λίστας"

#: inc/fonts.php:215
#, fuzzy
msgid "select paragraphs font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς παραγράφων"

#: inc/fonts.php:293
#, fuzzy
msgid "select paragraphs font weight"
msgstr "επιλέξτε παραγράφους βάρος γραμματοσειράς"

#: inc/layout.php:83
#, fuzzy
msgid "select rounded style"
msgstr "επιλέξτε στρογγυλεμένο στυλ"

#: inc/fonts.php:254
#, fuzzy
msgid "select table font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς πίνακα"

#: inc/fonts.php:332
#, fuzzy
msgid "select table font weight"
msgstr "επιλέξτε βάρος γραμματοσειράς πίνακα"

#: inc/colors.php:92
#, fuzzy
msgid "select theme kind"
msgstr "επιλέξτε είδος θέματος"

#: inc/fonts.php:267
#, fuzzy
msgid "select unordered list font size"
msgstr "επιλέξτε μέγεθος γραμματοσειράς μη ταξινομημένης λίστας"

#: inc/fonts.php:345
#, fuzzy
msgid "select unordered list font weight"
msgstr "select unordered list font weight"

#: inc/patterns/image.php:106
#, fuzzy
msgid "sepia image"
msgstr "εικόνα σέπια"

#: inc/images.php:181
#, fuzzy
msgid "sepia images"
msgstr "εικόνες σέπια"

#: classes/class-w3csspress-constants.php:153
#, fuzzy
msgid "serif"
msgstr "serif"

#: inc/content.php:80
#, fuzzy
msgid "set footer content."
msgstr "ορίστε το περιεχόμενο του υποσέλιδου."

#: functions.php:338
#, fuzzy
msgid "sidebar on the left."
msgstr "πλευρική γραμμή στα αριστερά."

#: functions.php:347
#, fuzzy
msgid "sidebar on the right."
msgstr "πλευρική γραμμή στα δεξιά."

#: inc/block-patterns.php:37
#, fuzzy
msgid "sidebars"
msgstr "πλευρικές γραμμές"

#: inc/content.php:20
#, fuzzy
msgid "skip to the content"
msgstr "μετάβαση στο περιεχόμενο"

#: classes/class-w3csspress-constants.php:117
#, fuzzy
msgid "small"
msgstr "μικρό"

#: search.php:65
#, fuzzy
msgid "Sorry, nothing matched your search. Please try again."
msgstr "Λυπάμαι, τίποτα δεν ταιριάζει με την αναζήτησή σας. Προσπαθήστε ξανά."

#: inc/speed.php:27
#, fuzzy
msgid "speed options"
msgstr "επιλογές ταχύτητας"

#: classes/class-w3csspress-constants.php:77
#, fuzzy
msgid "teal"
msgstr "πετρόλ"

#: inc/colors.php:107
#, fuzzy
msgid "text theme color"
msgstr "χρώμα θέματος κειμένου"

#: inc/patterns/query.php:44
#, fuzzy
msgid "the query did not return any results."
msgstr "το ερώτημα δεν επέστρεψε κανένα αποτέλεσμα."

#. thick border and direction
#: classes/class-w3csspress-constants.php:32
#, fuzzy, php-format
msgid "thick border %s"
msgstr "παχύ περίγραμμα %s"

#: classes/class-w3csspress-constants.php:171
#, fuzzy
msgid "three columns"
msgstr "τρεις στήλες"

#: classes/class-w3csspress-constants.php:116
#, fuzzy
msgid "tiny"
msgstr "μικροσκοπικό"

#: classes/class-w3csspress-constants.php:202
#, fuzzy
msgid "top"
msgstr "top"

#: classes/class-w3csspress-constants.php:170
#, fuzzy
msgid "two columns"
msgstr "δύο στήλες"

#. Description of the theme
#, fuzzy
msgid ""
"Universal theme with classic mode and blocks mode made to integrate the "
"W3CSS framework inside WordPress, with global custom options and all of the "
"W3CSS framework functionalities. The theme has been developed starting from "
"resources on the w3schools website applied to the blankslate WordPress theme,"
" slightly modified. The theme is accessibility ready and it is highly "
"customizable. Please add issues on GitHub or contact me if you are "
"interested in more options."
msgstr ""
"Καθολικό θέμα με κλασική λειτουργία και λειτουργία μπλοκ, φτιαγμένο για να "
"ενσωματώσει το πλαίσιο W3CSS μέσα στο WordPress, με παγκόσμιες "
"προσαρμοσμένες επιλογές και όλες τις λειτουργίες του πλαισίου W3CSS. Το θέμα "
"έχει αναπτυχθεί ξεκινώντας από τους πόρους του ιστότοπου w3schools που "
"εφαρμόζεται στο θέμα blankslate WordPress, ελαφρώς τροποποιημένο. Το θέμα "
"είναι έτοιμο για προσβασιμότητα και είναι εξαιρετικά προσαρμόσιμο. Παρακαλώ "
"προσθέστε θέματα στο GitHub ή επικοινωνήστε μαζί μου αν ενδιαφέρεστε για "
"περισσότερες επιλογές."

#: inc/fonts.php:203
#, fuzzy
msgid "use Google font"
msgstr "χρήση γραμματοσειράς Google"

#: inc/fonts.php:191
#, fuzzy
msgid "use Google font for headings"
msgstr "χρήση γραμματοσειράς Google για τίτλους"

#: inc/layout.php:84
#, fuzzy
msgid ""
"using this option you can change some elements roundness (images have more "
"controls)."
msgstr ""
"χρησιμοποιώντας αυτή την επιλογή μπορείτε να αλλάξετε τη στρογγυλότητα "
"ορισμένων στοιχείων (οι εικόνες έχουν περισσότερους ελέγχους)."

#: inc/layout.php:71
#, fuzzy
msgid "using this option you can change the page layout."
msgstr ""
"χρησιμοποιώντας αυτή την επιλογή μπορείτε να αλλάξετε τη διάταξη της σελίδας."

#: inc/colors.php:93
#, fuzzy
msgid ""
"using this option you can change the theme between default, light and dark."
msgstr ""
"χρησιμοποιώντας αυτή την επιλογή μπορείτε να αλλάξετε το θέμα μεταξύ "
"προεπιλεγμένου, ανοιχτού και σκοτεινού."

#: inc/colors.php:80
#, fuzzy
msgid "using this option you can change the theme colors."
msgstr ""
"χρησιμοποιώντας αυτή την επιλογή μπορείτε να αλλάξετε τα χρώματα του θέματος."

#: inc/layout.php:97
#, fuzzy
msgid "using this option you can enable or disable the grid layout."
msgstr ""
"χρησιμοποιώντας αυτή την επιλογή μπορείτε να ενεργοποιήσετε ή να "
"απενεργοποιήσετε τη διάταξη πλέγματος."

#. Name of the theme
#, fuzzy
msgid "W3CSSPress"
msgstr "W3CSSPress"

#: classes/class-w3csspress-constants.php:56
#, fuzzy
msgid "w3schools"
msgstr "w3schools"

#: classes/class-w3csspress-constants.php:138
#, fuzzy
msgid "weight"
msgstr "βάρος"

#: functions.php:365
#, fuzzy
msgid "widgets area on the foot of the website."
msgstr "περιοχή widgets στο κάτω μέρος του ιστότοπου."

#: functions.php:356
#, fuzzy
msgid "widgets area on the head of the website."
msgstr "περιοχή widgets στην κεφαλή του ιστότοπου."

#: classes/class-w3csspress-constants.php:120
#, fuzzy
msgid "XL"
msgstr "XL"

#: classes/class-w3csspress-constants.php:121
#, fuzzy
msgid "XXL"
msgstr "XXL"

#: classes/class-w3csspress-constants.php:122
#, fuzzy
msgid "XXXL"
msgstr "XXXL"

#: footer.php:27 inc/patterns/footer.php:22
#, fuzzy
msgid "Y"
msgstr "Y"

#: classes/class-w3csspress-constants.php:78
#, fuzzy
msgid "yellow"
msgstr "κίτρινο"
